import { fireEvent, render, screen } from "@testing-library/react";
import { Menu } from "./Menu";

describe("<Menu />", () => {
  it("calls passed method when edit is clicked", () => {
    const mockGoToEditMode = jest.fn();
    render(<Menu goToEditMode={mockGoToEditMode} />);
    fireEvent.click(screen.getByText("Edit"));
    expect(mockGoToEditMode).toHaveBeenCalled();
  });
});
