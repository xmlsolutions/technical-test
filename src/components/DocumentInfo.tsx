import "./DocumentInfo.css";
import { DocumentType, pageOptions } from "../App";

type DocumentInfoProps = {
  document: DocumentType;
  changePage?: (newPage: pageOptions) => void;
};

export const DocumentInfo = ({ document, changePage }: DocumentInfoProps) => {
  return (
    <div>
      <h4 className="formTitle">DOCUMENT INFORMATION</h4>
      <div
        className={changePage ? "formButton" : "formButton formInfo"}
        onClick={changePage ? () => changePage("selectType") : undefined}
      >
        <h4>TYPE</h4>
        <div style={{ margin: "auto" }} />
        <p>{document.typeDisplay}</p>
        {changePage && "\u1433"}
      </div>
      <div
        className={changePage ? "formButton" : "formButton formInfo"}
        onClick={changePage ? () => changePage("editDescription") : undefined}
      >
        <h4>DESCRIPTION</h4>
        <div style={{ margin: "auto" }} />
        <p>{document.description}</p>
        {changePage && "\u1433"}
      </div>
    </div>
  );
};
