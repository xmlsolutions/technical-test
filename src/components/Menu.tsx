type MenuProps = {
  goToEditMode: () => void;
};

export const Menu = ({ goToEditMode }: MenuProps) => {
  return (
    <div>
      <h4 className="formTitle">MENU OPTIONS</h4>
      <div className="formButton" onClick={goToEditMode}>
        <p>Edit</p>
        <div style={{ margin: "auto" }} />
        &#5171;
      </div>
    </div>
  );
};
