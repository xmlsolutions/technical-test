type EditDescriptionProps = {
  description: string;
  updateDescription: (newDescription: string) => void;
};

export const EditDescription = ({ description, updateDescription }: EditDescriptionProps) => {
  return (
    <textarea onChange={event => updateDescription(event.target.value)}>
      {description}
    </textarea>
  );
};
