import { render, screen } from "@testing-library/react";
import { InfoBanner } from "./InfoBanner";

describe("<InfoBanner />", () => {
  it("displays passed text", () => {
    render(<InfoBanner text={"test text"} />);
    expect(screen.getByText("test text")).toBeVisible();
  });
});
