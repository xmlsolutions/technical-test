type SelectTypeProps = {
  currentSelection: number;
  onSelection: (newType: SelectionType) => void;
};

export type SelectionType = {
  id: number;
  label: string;
};

export const SelectType = ({ currentSelection, onSelection }: SelectTypeProps) => {
  const types = [
    { id: 1, label: "Standard" },
    { id: 2, label: "System" },
  ];

  return (
    <>
      {types.map(type => (
        <div className="formButton" key={type.id} onClick={() => onSelection(type)}>
          <p>{type.label}</p>
          <div style={{ margin: "auto" }} />
          <p
            style={{
              color: currentSelection === type.id ? "#1b6ae0" : "inherit",
              fontWeight: "bold",
            }}
          >
            &#10003;
          </p>
        </div>
      ))}
    </>
  );
};
