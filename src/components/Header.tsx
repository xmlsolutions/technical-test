import "./Header.css";

type HeaderProps = {
  title: string;
  goBack?: () => void;
  rightButton?: { icon: string; onClick: () => void; disabled?: boolean };
};

export const Header = ({ title, goBack, rightButton }: HeaderProps) => {
  return (
    <header>
      {goBack && (
        <button onClick={goBack} className="headerButton" aria-label="back">
          &#129128;
        </button>
      )}
      <h2>{title}</h2>
      {rightButton && (
        <>
          <div style={{ margin: "auto" }} />
          <button
            onClick={rightButton.onClick}
            className="headerButton"
            disabled={rightButton.disabled}
          >
            {rightButton.icon}
          </button>
        </>
      )}
    </header>
  );
};
