import { fireEvent, render, screen } from "@testing-library/react";
import { SelectType } from "./SelectType";

const mockOnSelection = jest.fn();

describe("<SelectType />", () => {
  it("displays type options", () => {
    render(<SelectType currentSelection={1} onSelection={mockOnSelection} />);
    expect(screen.getByText("Standard")).toBeVisible();
    expect(screen.getByText("System")).toBeVisible();
  });

  it("displays the current selection with a blue tick", () => {
    render(<SelectType currentSelection={1} onSelection={mockOnSelection} />);
    expect(screen.getAllByText("\u2713")[0]).toHaveStyle("color: #1b6ae0");
  });

  it("displays the option not currently selected with a grey tick", () => {
    render(<SelectType currentSelection={1} onSelection={mockOnSelection} />);
    expect(screen.getAllByText("\u2713")[1]).toHaveStyle("color: inherit");
  });

  it("calls onSelection with the correct type id when type is clicked", () => {
    render(<SelectType currentSelection={1} onSelection={mockOnSelection} />);
    fireEvent.click(screen.getByText("System"));
    expect(mockOnSelection).toHaveBeenCalledWith({ id: 2, label: "System" });
  });
});
