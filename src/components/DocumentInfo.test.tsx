import { render, screen, fireEvent } from "@testing-library/react";
import { DocumentInfo } from "./DocumentInfo";

const testDocument = {
  id: 1,
  type: 1,
  typeDisplay: "test type",
  description: "test description",
};

const mockChangePage = jest.fn();

describe("<DocumentInfo />", () => {
  it("displays the correct document information", () => {
    render(<DocumentInfo document={testDocument} />);
    expect(screen.getByText("test type")).toBeVisible();
    expect(screen.getByText("test description")).toBeVisible();
  });

  it("displays extra icons in edit mode", () => {
    render(<DocumentInfo document={testDocument} changePage={mockChangePage} />);
    expect(screen.getAllByText("\u1433")).toHaveLength(2);
  });

  it("does not display extra icons when not in edit mode", () => {
    render(<DocumentInfo document={testDocument} />);
    expect(screen.queryAllByText("\u1433")).toHaveLength(0);
  });

  it("calls passed method when buttons are clicked", () => {
    render(<DocumentInfo document={testDocument} changePage={mockChangePage} />);
    fireEvent.click(screen.getByText("test type"));
    expect(mockChangePage).toHaveBeenCalledWith("selectType");
    mockChangePage.mockReset();
    fireEvent.click(screen.getByText("test description"));
    expect(mockChangePage).toHaveBeenCalledWith("editDescription");
  });
});
