type InfoBannerProps = {
  text: string;
};

export const InfoBanner = ({ text }: InfoBannerProps) => {
  return (
    <div style={{ padding: "8px", display: "flex" }}>
      &#128161;
      <p style={{ paddingLeft: "8px", margin: "0px" }}>{text}</p>
    </div>
  );
};
