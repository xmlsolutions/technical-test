import { fireEvent, render, screen } from "@testing-library/react";
import { Header } from "./Header";

const mockGoBack = jest.fn();
const mockOnClick = jest.fn();
const testSideButton = {
  onClick: mockOnClick,
  icon: "\u2713",
};

describe("<Header />", () => {
  it("displays the passed title", () => {
    render(<Header title={"test title"} />);
    expect(screen.getByText("test title")).toBeVisible();
  });

  it("displays back button when goBack method is passed", () => {
    render(<Header title={"test title"} goBack={mockGoBack} />);
    expect(screen.getByLabelText("back")).toBeVisible();
  });

  it("does not display back button when no goBack method is passed", () => {
    render(<Header title={"test title"} />);
    expect(screen.queryByLabelText("back")).not.toBeInTheDocument();
  });

  it("calls goBack when back button is clicked", () => {
    render(<Header title={"test title"} goBack={mockGoBack} />);
    fireEvent.click(screen.getByLabelText("back"));
    expect(mockGoBack).toHaveBeenCalled();
  });

  it("displays button on right side when a button is passed", () => {
    render(<Header title={"test title"} rightButton={testSideButton} />);
    expect(screen.getByText("\u2713")).toBeVisible();
  });

  it("calls associated method when right side button is clicked", () => {
    render(<Header title={"test title"} rightButton={testSideButton} />);
    fireEvent.click(screen.getByText("\u2713"));
    expect(mockOnClick).toHaveBeenCalled();
  });
});
