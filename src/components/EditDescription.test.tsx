import { render, screen, fireEvent } from "@testing-library/react";
import { EditDescription } from "./EditDescription";

const testDescription = "test description";
const mockUpdateDescription = jest.fn();

describe("<EditDescription />", () => {
  it("displays the correct description", () => {
    render(
      <EditDescription
        description={testDescription}
        updateDescription={mockUpdateDescription}
      />
    );
    expect(screen.getByText("test description")).toBeVisible();
  });

  it("calls updateDescription when description is changed", () => {
    render(
      <EditDescription
        description={testDescription}
        updateDescription={mockUpdateDescription}
      />
    );
    const textbox = screen.getByText("test description");
    fireEvent.change(textbox, { target: { value: "123" } });
    expect(mockUpdateDescription).toHaveBeenCalledWith("123");
  });
});
