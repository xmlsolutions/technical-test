import { useState } from "react";
import "./App.css";
import { DocumentInfo } from "./components/DocumentInfo";
import { EditDescription } from "./components/EditDescription";
import { Header } from "./components/Header";
import { InfoBanner } from "./components/InfoBanner";
import { Menu } from "./components/Menu";
import { SelectionType, SelectType } from "./components/SelectType";

const defaultDocument = {
  id: 287,
  description:
    "To be included as part of the Adult Admission Package. To be completed by doctors.",
  type: 1,
  typeDisplay: "Standard",
};

export type DocumentType = {
  id: number;
  description: string;
  type: number;
  typeDisplay: string;
};

export type pageOptions =
  | "detailsMode"
  | "menu"
  | "editMode"
  | "selectType"
  | "editDescription";

function App() {
  const [document, setDocument] = useState<DocumentType>(defaultDocument);
  const [changedDocument, setChangedDocument] = useState<DocumentType>(document);
  const [updatedDescription, setUpdatedDescription] = useState<string>(
    changedDocument.description
  );
  const [page, setPage] = useState<pageOptions>("detailsMode");

  return (
    <div>
      {page === "detailsMode" && (
        <>
          <Header
            title={"Document Details"}
            rightButton={{
              icon: "\u2630",
              onClick: () => {
                setPage("menu");
              },
            }}
          />
          <DocumentInfo document={document} />
        </>
      )}

      {page === "menu" && (
        <>
          <Header
            title={"Document Menu"}
            goBack={() => {
              setPage("detailsMode");
            }}
          />
          <InfoBanner text={"Below are the options available for this document."} />
          <Menu goToEditMode={() => setPage("editMode")} />
        </>
      )}

      {page === "editMode" && (
        <>
          <Header
            title={"Edit Document"}
            goBack={() => {
              setPage("detailsMode");
              setChangedDocument({ ...document });
              setUpdatedDescription(document.description);
            }}
            rightButton={{
              icon: "\u2713",
              onClick: () => {
                setPage("detailsMode");
                setDocument({ ...changedDocument });
              },
            }}
          />
          <InfoBanner text={"Make additions to the document below."} />
          <DocumentInfo
            document={changedDocument}
            changePage={(newPage: pageOptions) => setPage(newPage)}
          />
        </>
      )}

      {page === "selectType" && (
        <>
          <Header
            title={"Select Type"}
            goBack={() => {
              setPage("editMode");
            }}
          />
          <SelectType
            currentSelection={changedDocument.type}
            onSelection={(newType: SelectionType) => {
              setPage("editMode");
              setChangedDocument(currentdoc => ({
                ...currentdoc,
                type: newType.id,
                typeDisplay: newType.label,
              }));
            }}
          />
        </>
      )}

      {page === "editDescription" && (
        <>
          <Header
            title={"Enter Description"}
            goBack={() => {
              setPage("editMode");
              setUpdatedDescription(changedDocument.description);
            }}
            rightButton={{
              icon: "\u2713",
              onClick: () => {
                setPage("editMode");
                setChangedDocument(currentDoc => ({
                  ...currentDoc,
                  description: updatedDescription,
                }));
              },
              disabled: updatedDescription === changedDocument.description,
            }}
          />
          <EditDescription
            description={updatedDescription}
            updateDescription={(newDescription: string) =>
              setUpdatedDescription(newDescription)
            }
          />
        </>
      )}
    </div>
  );
}

export default App;
