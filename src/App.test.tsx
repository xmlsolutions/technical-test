import { fireEvent, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import App from "./App";

describe("<App />", () => {
  it("displays details mode by default", () => {
    render(<App />);
    expect(screen.getByText("Document Details")).toBeVisible();
  });

  it("changes to menu view when the menu icon is selected", () => {
    render(<App />);
    fireEvent.click(screen.getByText("\u2630"));
    expect(screen.getByText("Document Menu")).toBeVisible();
    expect(
      screen.getByText("Below are the options available for this document.")
    ).toBeVisible();
  });

  it("returns to the home screen from menu view when the back arrow is clicked", () => {
    render(<App />);
    fireEvent.click(screen.getByText("\u2630"));
    fireEvent.click(screen.getByLabelText("back"));
    expect(screen.getByText("Document Details")).toBeVisible();
  });

  it("changes from menu view to edit view when Edit is clicked", () => {
    render(<App />);
    fireEvent.click(screen.getByText("\u2630"));
    fireEvent.click(screen.getByText("Edit"));
    expect(screen.getByText("Edit Document")).toBeVisible();
    expect(screen.getByText("Make additions to the document below.")).toBeVisible();
  });

  it("returns to the home screen from edit view when the back arrow is clicked", () => {
    render(<App />);
    fireEvent.click(screen.getByText("\u2630"));
    fireEvent.click(screen.getByText("Edit"));
    fireEvent.click(screen.getByLabelText("back"));
    expect(screen.getByText("Document Details")).toBeVisible();
  });

  it("changes to select type view when type button is clicked", () => {
    render(<App />);
    fireEvent.click(screen.getByText("\u2630"));
    fireEvent.click(screen.getByText("Edit"));
    fireEvent.click(screen.getByText("Standard"));
    expect(screen.getByText("Select Type")).toBeVisible();
  });

  it("returns to edit view when a new type is selected", () => {
    render(<App />);
    fireEvent.click(screen.getByText("\u2630"));
    fireEvent.click(screen.getByText("Edit"));
    fireEvent.click(screen.getByText("Standard"));
    fireEvent.click(screen.getByText("System"));
    expect(screen.getByText("Edit Document")).toBeVisible();
    expect(screen.getByText("System")).toBeVisible();
  });

  it("changes to edit description view when description button is clicked", () => {
    render(<App />);
    fireEvent.click(screen.getByText("\u2630"));
    fireEvent.click(screen.getByText("Edit"));
    fireEvent.click(screen.getByText(/To be included as part of/));
    expect(screen.getByText("Enter Description")).toBeVisible();
  });

  it("ignores changes to description when user returns to edit view", () => {
    render(<App />);
    fireEvent.click(screen.getByText("\u2630"));
    fireEvent.click(screen.getByText("Edit"));
    fireEvent.click(screen.getByText(/To be included as part of/));
    fireEvent.change(screen.getByText(/To be included as part of/), "123");
    fireEvent.click(screen.getByLabelText("back"));
    expect(screen.getByText(/To be included as part of/)).toBeVisible();
  });

  it("saves changes to description when tick icon is clicked", () => {
    render(<App />);
    fireEvent.click(screen.getByText("\u2630"));
    fireEvent.click(screen.getByText("Edit"));
    fireEvent.click(screen.getByText(/To be included as part of/));
    userEvent.type(screen.getByText(/To be included as part of/), "123");
    fireEvent.click(screen.getByText("\u2713"));
    expect(screen.getByText(/123/)).toBeVisible();
  });

  it("allows user to save all edits to the document", () => {
    render(<App />);
    fireEvent.click(screen.getByText("\u2630"));
    fireEvent.click(screen.getByText("Edit"));
    fireEvent.click(screen.getByText("Standard"));
    fireEvent.click(screen.getByText("System"));
    fireEvent.click(screen.getByText(/To be included as part of/));
    userEvent.type(screen.getByText(/To be included as part of/), "123");
    fireEvent.click(screen.getByText("\u2713"));
    fireEvent.click(screen.getByText("\u2713"));
    expect(screen.getByText("Document Details")).toBeVisible();
    expect(screen.getByText("System")).toBeVisible();
    expect(screen.getByText(/123/)).toBeVisible();
  });

  it("removes all edits to the document if the user returns to the details page witohut saving", () => {
    render(<App />);
    fireEvent.click(screen.getByText("\u2630"));
    fireEvent.click(screen.getByText("Edit"));
    fireEvent.click(screen.getByText("Standard"));
    fireEvent.click(screen.getByText("System"));
    fireEvent.click(screen.getByText(/To be included as part of/));
    userEvent.type(screen.getByText(/To be included as part of/), "123");
    fireEvent.click(screen.getByText("\u2713"));
    fireEvent.click(screen.getByLabelText("back"));
    expect(screen.getByText("Document Details")).toBeVisible();
    expect(screen.getByText("Standard")).toBeVisible();
    expect(screen.queryByText(/123/)).not.toBeInTheDocument();
  });
});
