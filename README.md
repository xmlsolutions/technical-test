## Running the app

- Download the repository
  ```console
  git clone https://git@bitbucket.org/xmlsolutions/technical-test.git
  ```
- Change into the app directory
  ```console
  cd technical-test
  ```
- Install the packages
  ```console
  npm install
  ```
- Start running the app
  ```console
  npm start
  ```
- The app should open in the browser. If not, then you can navigate to http://localhost:3000/ to view the app.

## Running the tests

From the app directory run:

```console
npm test
```

This will run all tests and print out the results.

## UI

The app is best viewed in mobile mode in the browser. This has been done to match the narrow view given in the task description examples.
